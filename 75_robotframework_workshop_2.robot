***Settings***
Suite Teardown  Close All Browsers
Library  BuiltIn
Library  Collections
Library  SeleniumLibrary
***Variables***
${url} =  https://ptt-devpool-robotframework.herokuapp.com
${userName} =  admin
${passWord} =  1234
&{data1} =  FirstName=David  LastName=Beckham  Email=David.Beckham@gmail.com
&{data2} =  FirstName=Johnny  LastName=Depp  Email=Johnny.Depp@gmail.com
&{data3} =  FirstName=Robert  LastName=Downey  Email=Robert.Downey@gmail.com
&{data4} =  FirstName=Tom  LastName=Hanks  Email=Tom.Hanks@gmail.com
&{data5} =  FirstName=Suchuj  LastName=Kaenasa  Email=suchujkaenasa@gmail.com
@{list} =  ${data1}  ${data2}  ${data3}  ${data4}  ${data5}

***Test Cases***
0️⃣ เปิด Browser (Chrome) 0️⃣
    Open Browser  about:blank  browser=gc
    Maximize Browser Window
    Set Selenium Speed  0.5
1️⃣ เปิดเว็บไซต์ https://ptt-devpool-robotframework.herokuapp.com 1️⃣
    Go To  ${url}
2️⃣ ระบุ Username และ Password โดยใช้ข้อมูลดังนี้ Username=admin, Password=1234 2️⃣
    Input Text  id:txt_username  ${userName}
    Input Text  id:txt_password  ${passWord}
3️⃣ คลิกปุ่ม Sign in 3️⃣
    Click Button  id:btn_login
4️⃣ เพิ่มข้อมูล ด้วยปุ่ม New Data 4️⃣
    NewDatas  ${list}
5️⃣ Capture Screenshot 5️⃣
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png
***Keywords***
NewDatas
    [Arguments]  ${paramTable}
    FOR  ${row}  IN  @{paramTable}
        Click Button  id:btn_new
        AddDatas  ${row}
    END
AddDatas
    [Arguments]  ${colum}
    FOR  ${Stack}  IN  ${colum}
        Input Text  id:txt_new_firstname  ${Stack}[FirstName]
        Input Text  id:txt_new_lastname  ${Stack}[LastName]
        Input Text  id:txt_new_email  ${Stack}[Email]
        Click Button  id:btn_new_save
    END